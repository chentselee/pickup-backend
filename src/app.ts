import express from "express";
import { routers } from "./routers";
import { db } from "./db";
import { errorHandler } from "./error";

db.onConnect(() => {
  const app = express();

  const port = 8000;

  app.use(express.json());

  routers.forEach((router) => {
    app.use(router);
  });

  app.get("/", (_, req) => {
    req.send("<h1>hello world</h1>");
  });

  app.use(errorHandler);

  app.listen(port, () => {
    console.log(`Server listening on port ${port}`);
  });
});
