import { getRepository } from "typeorm";
import { ApiError, ApiErrorType } from "src/error";
import { User, UserInput } from "./type";
import { UserEntity } from "./entity";

const parseUserEntity = (user: UserEntity): User => ({
  ...user,
  createdAt: user.createdAt.toISOString(),
  updatedAt: user.updatedAt.toISOString(),
});

export const getUsers = async () => {
  const userEntity = await getRepository(UserEntity).find();
  return userEntity.map(parseUserEntity);
};

export const getUser = async (id: User["id"]) => {
  const userEntity = await getRepository(UserEntity).findOne({ where: { id } });
  if (!userEntity) {
    throw new ApiError({ type: ApiErrorType.NotFound });
  } else {
    return parseUserEntity(userEntity);
  }
};

export const createUser = async (userInput: UserInput) => {
  const result = await getRepository(UserEntity).insert(userInput);
  const userEntity = await getRepository(UserEntity).findOne({
    where: { id: result.identifiers[0].id },
  });
  if (!userEntity) {
    throw new ApiError({
      type: ApiErrorType.CreateUserError,
      message: "Create user error",
    });
  } else {
  }
  return userEntity ? parseUserEntity(userEntity) : undefined;
};

export const updateUser = async (
  id: User["id"],
  userInput: Partial<UserInput>
) => {
  const userEntity = await getRepository(UserEntity).findOne({
    where: { id },
  });
  if (!userEntity) {
    throw new ApiError({ type: ApiErrorType.NotFound });
  } else {
    await getRepository(UserEntity).update({ id }, userInput);
    return parseUserEntity(userEntity);
  }
};

export const deleteUser = async (id: User["id"]) => {
  const userEntity = await getRepository(UserEntity).findOne({
    where: { id },
  });
  if (!userEntity) {
    throw new ApiError({ type: ApiErrorType.NotFound });
  } else {
    await getRepository(UserEntity).delete({ id });
    return parseUserEntity(userEntity);
  }
};
