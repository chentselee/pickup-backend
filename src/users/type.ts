export interface User {
  id: string;
  name: string;
  gender: "male" | "female" | "other";
  age: number;
  createdAt: string;
  updatedAt: string;
}

export type UserInput = Pick<User, "name" | "gender" | "age">;
