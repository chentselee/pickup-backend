import { Router } from "express";
import {
  handleCreateUser,
  handleDeleteUser,
  handleGetUser,
  handleGetUsers,
  handleUpdateUser,
} from "./controller";

export const router = Router();

router.get("/users", handleGetUsers);
router.get("/users/:userId", handleGetUser);
router.post("/users", handleCreateUser);
router.put("/users/:userId", handleUpdateUser);
router.delete("/users/:userId", handleDeleteUser);
