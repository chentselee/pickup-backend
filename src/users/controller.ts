import { RequestHandler } from "express";
import {
  getUsers,
  getUser,
  createUser,
  updateUser,
  deleteUser,
} from "./service";
import { User, UserInput } from "./type";

export const handleGetUsers: RequestHandler<unknown, User[]> = async (
  _,
  res
) => {
  res.json(await getUsers());
};

export const handleGetUser: RequestHandler<{ userId: string }, User> = async (
  req,
  res,
  next
) => {
  try {
    res.json(await getUser(req.params.userId));
  } catch (error) {
    next(error);
  }
};

export const handleCreateUser: RequestHandler<unknown, User, UserInput> =
  async (req, res, next) => {
    try {
      res.json(await createUser(req.body));
    } catch (error) {
      next(error);
    }
  };

export const handleUpdateUser: RequestHandler<
  { userId: string },
  User,
  Partial<UserInput>
> = async (req, res, next) => {
  try {
    res.json(await updateUser(req.params.userId, req.body));
  } catch (error) {
    next(error);
  }
};

export const handleDeleteUser: RequestHandler<{ userId: string }, User> =
  async (req, res, next) => {
    try {
      res.json(await deleteUser(req.params.userId));
    } catch (error) {
      next(error);
    }
  };
