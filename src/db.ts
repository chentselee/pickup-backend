import { createConnection } from "typeorm";
import "reflect-metadata";

export const db = {
  onConnect: async (callback: Function) => {
    await createConnection();
    callback();
  },
};
