import { ErrorRequestHandler } from "express";

export enum ApiErrorType {
  NotFound = "NotFound",
  CreateUserError = "CreateUserError",
}

export class ApiError {
  type: ApiErrorType;
  message?: string;

  constructor(newApiError: { type: ApiErrorType; message?: string }) {
    this.type = newApiError.type;
    this.message = newApiError.message;
  }
}

export const errorHandler: ErrorRequestHandler = (
  error: ApiError,
  _,
  res,
  next
) => {
  switch (error.type) {
    case ApiErrorType.NotFound:
      res.status(404).json({ type: error.type, message: "Not found" });
      break;
    case ApiErrorType.CreateUserError:
      res.status(400).json({ type: error.type });
      break;
    default:
      next(error);
  }
};
